# =============================================================================
# 15-11-2021
# =============================================================================
## importing libraries
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import warnings

warnings.filterwarnings('ignore')

pd.set_option('display.max_columns', None)

from datetime import datetime
from datetime import date

## importing the mysql fetch function 
from fetch import *

####################### Features ####################### 
# =============================================================================
# * Age
# * sex
# * income_per_annum
# * occupation
# * number of logins in last year
# * num_trades in last year
# * years with iifl
# * days since last login
# * days since last trade
# * online%
# * FAO segment
# * dp_holding
# * ledger balance
# * Z20
# * brokerage Total
# * app_status
# * E2 channel
# * iskarvy
# * KRA
# =============================================================================

## importing source files
df_client = pd.read_csv("../../../data/dw_mst_client.csv")
df_app_clients = pd.read_csv("../../../data/app_clients.csv")

df_FAO = pd.read_csv("../../../data/FAO_clients.csv")
df_ledger = pd.read_csv("../../../data/THV_ALB_all_clients_20211114.csv")
df_etf = pd.read_csv("../../../data/etf_accepted_till_20211115.csv")

df_z20 = pd.read_csv("../../../data/z20_20211115.csv")


df_client =  df_client[df_client["data_source"] == "Equity"]


######## function to add gather data
def data_summon(df_client, df_etf, df_ledger, df_z20):
    
# =============================================================================
#     print("======================= data importing =========================")
#     df_client = pd.read_csv("../../../data/dw_mst_client.csv")
#     df_app_clients = pd.read_csv("../../../data/app_clients.csv")
#     
#     df_FAO = pd.read_csv("../../../data/FAO_clients.csv")
#     df_ledger = pd.read_csv("../../../data/THV_ALB_all_clients_20211114.csv")
#     df_etf = pd.read_csv("../../../data/etf_accepted_till_20211115.csv")
#     
#     df_z20 = pd.read_csv("../../../data/z20_20211115.csv")
#     
#     
#     df_client =  df_client[df_client["data_source"] == "Equity"]
#     
# =============================================================================
    
    
    
    
    print("=================================================")
    print("data gathering started")
    # =============================================================================
    # ## adding demographics and target variable(ETF yes/no)
    df_model = pd.merge(df_client, df_etf, 
                        left_on = "cm_cd", 
                        right_on = "ClientCode", 
                        how = "left")[["cm_cd", "cm_sex", "cm_dob", 
                                    "IncomePA",
                                    "Last_MobileLoginDate","E2Channel", "IsKarvy", "KRA YN", "ETF", "LTD"]]
    print("Added demographics and target variable!!") 
    print("=============================================")      
    df_model["ETF"] = df_model["ETF"].fillna(0)
    # =============================================================================
    
    ## adding age column
    df_model["cm_dob"] = df_model["cm_dob"].astype(str).str.split(".").str[0]
    df_model["cm_dob"] = pd.to_datetime(df_model["cm_dob"], errors = "coerce")
    now = pd.to_datetime('now')

    df_model["age"] = (now - df_model['cm_dob']).astype('<m8[Y]') 
    del df_model["age"]
    
              
    # =============================================================================
    # quering data for trades in last one year
    now = datetime.now()

    query = """select 
    	Client_Code, 
    	sum(Trades#) as trades_in_one_year
    from 
    	dw_mis_broking_details
    where
    	trn_date > {}
    group by 
    	Client_Code""".format(str(now.year - 1) + '{:02d}'.format(now.month) + "01")
    df_trades_one_year = get_data(query)
    df_trades_one_year.to_csv("../../../data/trades_one_year_from_{}".format(str(now.year) + '{:02d}'.format(now.month) + "01"))
    print("Trades queried!!")
    print("trades data shape: {}, {}".format(df_trades_one_year.shape[0], df_trades_one_year.shape[1]))
    # =============================================================================
                              

    ## adding trades data to df_model 
    df_model = pd.merge(df_model, df_trades_one_year, 
                        left_on = "cm_cd", 
                        right_on = "Client_Code", how = "left")[list(df_model.columns) + ["trades_in_one_year"]]     


    df_model["trades_in_one_year"].fillna(0)
    
    print("=============================================")    
    print("current shape is: {}, {}".format(df_model.shape[0], df_model.shape[1]))
    
    # =============================================================================
    # quering data for logins in last one year
    now = datetime.now()

    query = """select 
    	ClientCode, 
    	sum(Logins#) as logins_in_one_year
    from 
    	dw_mis_mobile_login_dates
    where
    	LoginDates > {}
    group by 
    	ClientCode""".format(str(now.year - 1) + '{:02d}'.format(now.month) + "01")
    df_logins_one_year = get_data(query)
    df_logins.to_csv("../../../data/logins_one_year_from_{}".format(str(now.year) + '{:02d}'.format(now.month) + "01"))
    print("Logins queried!!")
    print("logins data shape: {}, {}".format(df_logins_one_year.shape[0], df_logins_one_year.shape[1]))
    # =============================================================================
                                           
    ## adding logins data to df_model 
    df_model = pd.merge(
        df_model, df_logins_one_year, 
        left_on = "cm_cd", 
        right_on = "ClientCode", how = "left")[list(df_model.columns) + ["logins_in_one_year"]]     

    df_model["logins_in_one_year"].fillna(0)
    
    print("=============================================")    
    print("current shape is: {}, {}".format(df_model.shape[0], df_model.shape[1]))
    
    
    ## adding z20 tag
    df_z20.rename(columns = {"cm_cd": "client_code"}, inplace = True)
    df_z20["Z20"] = [1] * len(df_z20)
    df_model = pd.merge(
        df_model, df_z20, 
        left_on = "cm_cd", 
        right_on = "client_code", how = "left")[list(df_model.columns) + ["z20"]]     

    df_model["z20"].fillna(0)   
    df_model["z20"].value_counts()
    
    print("Added z20 tag!!")
    print("=============================================")    
    print("current shape is: {}, {}".format(df_model.shape[0], df_model.shape[1]))
    
    
    ## adding ledger balance and dp holding
    df_model = pd.merge(
        df_model, df_ledger, 
        left_on = "cm_cd", 
        right_on = "LoginID", how = "left")[list(df_model.columns) + ["THV", "ALB"]]     

    df_model["THV"].fillna(0)   
    df_model["ALB"].fillna(0) 
    print("Added THV and ALB tag!!")
    print("=============================================")    
    print("current shape is: {}, {}".format(df_model.shape[0], df_model.shape[1]))
    
    
    ## adding num of months since last trade
    df_model["Last_MobileLoginDate"] = df_model["Last_MobileLoginDate"].astype(str).str.split(".").str[0]
    df_model["Last_MobileLoginDate"] = pd.to_datetime(df_model["Last_MobileLoginDate"], errors = "coerce")
    df_model["months_since_last_trade"] = (now - df_model['Last_MobileLoginDate'])/np.timedelta64(1, 'M')
    del df_model["Last_MobileLoginDate"]
    
    
    ## adding num of months since last login
    df_model["LTD"] = df_model["LTD"].astype(str).str.split(".").str[0]
    df_model["LTD"] = pd.to_datetime(df_model["LTD"], errors = "coerce")
    df_model["months_since_last_trade"] = (now - df_model['LTD'])/np.timedelta64(1, 'M')
    del df_model["LTD"]
    
    return df_model
df_model = data_summon(df_client, df_etf, df_ledger, df_z20)

    
